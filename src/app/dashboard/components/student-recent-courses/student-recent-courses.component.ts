import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-student-recent-courses',
  templateUrl: './student-recent-courses.component.html',
  styleUrls: ['./student-recent-courses.component.scss']
})
export class StudentRecentCoursesComponent implements OnInit {

  public recentCourses: any = [];
  public isLoading = true;   // Only if data is loaded

  constructor(private _context: AngularDataContext) {
  }

  ngOnInit() {
    this.getCurrentRegistration().then(res => {
      // Order asc by couse name
      this.recentCourses = (res && res.classes)  ? res.classes.sort( (x, y) => x.course.name.localeCompare(y.course.name) ) : [];
      this.isLoading = false; // Data is loaded
    });
  }


  getCurrentRegistration() {
    return this._context.model('students/me/currentRegistration')
      .asQueryable()
      .expand('classes($expand=course,courseClass($expand=instructors($expand=instructor($select=id,givenName,familyName,category))),courseType)')
      .getItem()
      .then(currentRegistration => {
        return currentRegistration;
      }).catch(err => {
        return [];
      });
  }

}
