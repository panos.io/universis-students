import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentRecentGradesComponent } from './student-recent-grades.component';

describe('StudentRecentGradesComponent', () => {
  let component: StudentRecentGradesComponent;
  let fixture: ComponentFixture<StudentRecentGradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentRecentGradesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentRecentGradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
