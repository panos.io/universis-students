export const navigation = [
  {
    title: true,
    name: 'Main Menu'
  },
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
  },
  {
    name: 'Δηλώσεις',
    url: '/registrations',
    icon: 'icon-puzzle',
    children: [
      {
        name:  'Δήλωση Εξαμήνου',
        url: '/registrations/semester',
        icon: 'icon-puzzle'
      },
      {
        name: 'Δήλωση Μαθημάτων',
        url: '/registrations/courses',
        icon: 'icon-puzzle'
      },
      {
        name: 'Προηγούμενες Δηλώσεις',
        url: '/registrations/list',
        icon: 'icon-puzzle'
      },
    ]
  },

  {
    name: 'Βαθμολογία',
    url: '/grades',
    icon: 'icon-book-open',
    children: [
      {
        name: 'Πρόσφατη Βαθμολογία',
        url: '/grades/recent',
        icon: 'icon-puzzle'
      },
      {
        name: 'Αναλυτική Βαθμολογία',
        url: '/grades/all',
        icon: 'icon-puzzle'
      },
      {
        name: 'Εργασίες',
        url: '/grades/project',
        icon: 'icon-puzzle'
      },
    ]
  },

  {
    name: 'Αιτήσεις',
    url: '/requests/list',
    icon: 'icon-note',

  },

  {
    divider: true
  },
  {
    title: true,
    name: 'Μενου Χρήστη',
  },
  {
    name: 'Προφίλ',
    url: '/profile',
    icon: 'icon-user',

  },

  {
    name: 'Γλώσσα',
    url: '/lang',
    icon: 'icon-globe',
    children: [
      {
        name: 'Ελληνικά',
        url: '/lang/el',
        icon: 'icon-cursor'
      },
      {
        name: 'English',
        url: '/lang/en',
        icon: 'icon-cursor'
      },
    ]

  },
  {
    name: 'Αποσύνδεση',
    url: '/auth/logout',
    icon: 'icon-lock',
    variant: 'notice'
  }
];
