import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {ConfigurationService} from "./services/configuration.service";
import {ModalService} from './services/modal.service';
import {MsgboxComponent} from './msgbox/msgbox.component';
import {LangswitchComponent} from './langswitch/langswitch.component';
import {LocalizedDatePipe} from "./localized-date.pipe";
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {LangComponent} from "./lang-component";
import {ModalModule} from 'ngx-bootstrap';
import {DialogComponent} from './modals/dialog.component';
import { SpinnerComponent } from './modals/spinner.component';
import { NgSpinKitModule } from 'ng-spin-kit';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        BsDropdownModule,
        ModalModule,
        NgSpinKitModule
    ],
    providers: [
        ConfigurationService,
        ModalService
    ],
    declarations: [
        LocalizedDatePipe,
        MsgboxComponent,
        LangComponent,
        LangswitchComponent,
        DialogComponent,
        SpinnerComponent],
    entryComponents: [
        DialogComponent,
        SpinnerComponent
    ],
    exports: [
        MsgboxComponent,
        LangswitchComponent,
        LangComponent,
        DialogComponent,
        LocalizedDatePipe]
})
export class SharedModule {
}
