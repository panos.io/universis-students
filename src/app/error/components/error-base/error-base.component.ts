import {Component, OnDestroy, OnInit, Renderer2, ViewEncapsulation} from '@angular/core';
import { ErrorService } from '../../error.service';
import { TranslateService } from "@ngx-translate/core";
import { ConfigurationService } from "../../../shared/services/configuration.service";
import { ActivatedRoute, Router } from "@angular/router";

declare var $: any;

@Component({
  selector: 'app-error-base',
  templateUrl: './error-base.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./error-base.component.scss']
})
export class ErrorBaseComponent implements OnInit, OnDestroy {


  public code: string;
  public title: string;
  public message: string;
  public continue: string;
  public action: string;

  constructor(public config: ConfigurationService,
            protected _errorService: ErrorService,
            protected _translateService: TranslateService,
              protected _renderer: Renderer2) {

      //add center content classes
      this._renderer.addClass(document.body, 'flex-row');
      this._renderer.addClass(document.body, 'align-items-center');
    //get last error
    let error = this._errorService.getLastError();
    //check error.code property
    if (error && typeof error.code === 'string') {
      this.code = error.code;
    }
    //check error.status or error.statCode property
    else if (error && typeof (error.status || error.statusCode) === 'number') {
      this.code = `E${error.status || error.statusCode}`;
    }
    //default error code (E500)
    else {
      this.code = 'E500';
    }
    if (error && typeof error.continue === 'string') {
      this.continue = error.continue;
    }
    else {
      this.continue = "/";
    }
    this.action = "Error.Continue";

  }

    get displayCode() {
        if (/^E(\d+)\.?(\d+)?$/g.test(this.code)) {
            return this.code;
        }
        return "E500";
    }

  ngOnInit() {


    this._translateService.get(this.code).subscribe((translation) => {
      if (translation) {
        this.title = translation.title;
        this.message = translation.message;
      }
      else {
        this._translateService.get('E500').subscribe((translation) => {
          this.title = translation.title;
          this.message = translation.message;
        });
      }
    });
  }

    ngOnDestroy(): void {
      //remove center content classes
        this._renderer.removeClass(document.body, 'flex-row');
        this._renderer.removeClass(document.body, 'align-items-center');
    }
}

@Component({
  selector: '.m-error.m-error--http',
  templateUrl: './error-base.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./error-base.component.scss']
})
export class HttpErrorComponent extends ErrorBaseComponent {
  constructor(public config: ConfigurationService,
    protected _errorService: ErrorService,
    protected _translateService: TranslateService,
    private _router: Router,
    private _route: ActivatedRoute,
              protected _renderer: Renderer2) {
    super(config, _errorService, _translateService, _renderer);
  }

  ngOnInit() {
    this._route.params.subscribe((params) => {
      if (params.status) {
        this.code = `E${params.status}`;
      }
      this._route.queryParams.subscribe((queryParams) => {
        if (queryParams.continue) {
          this.continue = queryParams.continue;
        }
        if (queryParams.action) {
          this.action = queryParams.action;
        }
        this._translateService.get(this.code || 'E500').subscribe((translation) => {
          if (translation) {
            this.title = translation.title;
            this.message = translation.message;
          }
        });
      });

    });
  }
}
