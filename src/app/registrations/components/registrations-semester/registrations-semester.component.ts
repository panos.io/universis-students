import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CurrentRegistrationService, REGISTRATION_STATUS} from '../../services/currentRegistrationService.service';
import {ProfileService} from '../../../profile/services/profile.service';
import {TranslateService} from '@ngx-translate/core';
import {LoadingService} from 'src/app/shared/services/loading.service';
import {LocalizedDatePipe} from 'src/app/shared/localized-date.pipe';
import {ErrorService} from '../../../error/error.service';

@Component({
  selector: 'app-registrations-semester',
  templateUrl: 'registrations-semester.component.html'
})
export class RegistrationSemesterComponent implements OnInit {

  params: any = {};
  message: any;
  action: any;
  disableButton: boolean;

  constructor(private _currentRegistrationService: CurrentRegistrationService,
              private _translateService: TranslateService,
              private _profileService: ProfileService,
              private _router: Router,
              private _loading: LoadingService,
              private _errorService: ErrorService) { }

  async ngOnInit() {
    this._loading.showLoading();
    try {
        const student = await this._profileService.getStudent();
        if (student &&
            student.department &&
            student.department.organization &&
            student.department.organization.instituteConfiguration &&
            !student.department.organization.instituteConfiguration.useStudentRegisterAction) {
            return this._router.navigate(['/registrations/courses']);
        }
        // get current registration status
        const registrationStatus = await this._currentRegistrationService.getRegistrationStatus();
        // get register action for current academic period
        // note: student institute uses student register actions
        // so first of all we should check if student has been registered
        const registerAction = await this._currentRegistrationService.getCurrentRegisterAction();
        // get registration period start and end formatter
        const formatter = new LocalizedDatePipe(this._translateService);
        // get registration period info for current academic period
        const registrationPeriodInfo = student.department.organization.registrationPeriods.find( x => {
            return x.academicPeriod === student.department.currentPeriod.id &&
                x.academicYear === student.department.currentYear.id;
        });
        let statusText;
        let validPeriod = false;
        // if registration period has been set yet (and register action is null)
        if (registerAction == null) {
            if (registrationPeriodInfo == null) {
                // registration period is not available at all
                statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.NotAvailable);
                // set messages
                this.message = this._translateService.instant(`SemesterRegistration.${statusText}`, this.params);
                // hide loading
                this._loading.hideLoading();
                // and return
                return;
            }
            // check registration period info dates
            const currentDate = new Date();
            validPeriod = registrationPeriodInfo.registrationPeriodStart <= currentDate &&
                currentDate <= registrationPeriodInfo.registrationPeriodEnd;
            if (!validPeriod) {
              // check if semester registration period is passed
              if ( currentDate > registrationPeriodInfo.registrationPeriodEnd) {
                statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.Deadline);
              } else if ( currentDate < registrationPeriodInfo.registrationPeriodStart) {
                // check if registration period is coming
                statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.NotAvailableTill);
              }
            }
        } else {
          // register action exists
          statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.Successful);
        }
        this.params = Object.assign({
            'registrationPeriodStartDate': formatter.transform(registrationPeriodInfo.registrationPeriodStart, 'mediumDate'),
            'registrationPeriodEndDate': formatter.transform(registrationPeriodInfo.registrationPeriodEnd, 'mediumDate')
        }, student.department);
         // check also period registration status
          const effectiveStatus = await this._currentRegistrationService.getCurrentRegistrationEffectiveStatus();
          // get messages according to current registration status
      if ( !statusText) {
          if (registerAction == null && registrationStatus === REGISTRATION_STATUS.Successful) {
            statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.Available);
          } else {
            statusText = REGISTRATION_STATUS.toString(registrationStatus);
          }
      }
          this.message = {
            'title': student.department.currentPeriod.name + ' ' + student.department.currentYear.name,
            'icon': `SemesterRegistration.${statusText}.icon`,
            'info': `SemesterRegistration.${statusText}.info`,
            'message': `SemesterRegistration.${statusText}.message`,
            'extraMessage': `SemesterRegistration.${statusText}.extraMessage`,
            'actionButton': `SemesterRegistration.${statusText}.actionButton`,
            'actionText': `SemesterRegistration.${statusText}.actionText`,
            'action': `SemesterRegistration.${statusText}.action`
        };
        const courses = (effectiveStatus.code === 'OPEN_NO_TRANSACTION'
          || effectiveStatus.code ===  'PENDING_TRANSACTION'
          || effectiveStatus.code ===  'SUCCEEDED_TRANSACTION'
          || effectiveStatus.code ===  'FAILED_TRANSACTION'
          || effectiveStatus.code === 'P_SUCCEEDED_TRANSACTION'
          || effectiveStatus.code ===  'P_SYSTEM_TRANSACTION');
        // Get Action for button
        this.action = this._translateService.instant(this.message.action);
        this.disableButton = !courses;
        this._loading.hideLoading();
    } catch  (err) {
        this._loading.hideLoading();
        // noinspection JSIgnoredPromiseFromCall
        this._errorService.navigateToError(err);
    }
  }

  open() {
    switch (this.action) {
      case '1':
        this.registerSemester();
        break;
      case '2':
        this.registerCourses();
        break;
    }
  }

  registerSemester() {
    this._currentRegistrationService.registerSemester().then(() => {
        return this._router.navigateByUrl('/registrations/courses');
    }).catch( err => {
        // noinspection JSIgnoredPromiseFromCall
        this._errorService.navigateToError(err);
    });
  }

  registerCourses() {
        // noinspection JSIgnoredPromiseFromCall
      this._router.navigateByUrl('/registrations/courses');
  }
}
