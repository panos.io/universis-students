import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {MostModule} from "@themost/angular";
import {environment} from "../../environments/environment";
import {SharedModule} from "../shared/shared.module";
import {GradePipe, GradeScaleService} from "./services/grade-scale.service";
import {GradesService} from "./services/grades.service";

@NgModule({
  imports: [
      CommonModule,
      TranslateModule,
      MostModule,
      SharedModule
  ],
  declarations: [
      GradePipe
  ],
    exports: [
        GradePipe
    ],
    "providers": [
      GradeScaleService,
      GradesService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GradesSharedModule {

    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/grades.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
