import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {GradeScale, GradeScaleService} from "./grade-scale.service";

@Injectable()
export class GradesService {

  constructor(private _context: AngularDataContext, private _gradeScaleService: GradeScaleService) { }

    getAllGrades(): any {
        return this._context.model('students/me/grades')
            .asQueryable()
            .expand('course, courseExam($expand=examPeriod,year)')
            .orderByDescending('courseExam/year')
            .thenByDescending('courseExam/examPeriod')
            .thenByDescending('grade1')
            .take(-1)
            .getItems();
    }

    getCourseTeachers(): any {
        return this._context.model('students/me/classes')
            .asQueryable()
            .select('course/id as id, courseClass')
            .expand('courseClass($expand=instructors($expand=instructor))')
            .take(-1)
            .getItems();
    }

    getGradeInfo(): any {
        return this._context.model('students/me/courses')
            .asQueryable()
            .expand('course', 'gradeExam($expand=instructors($expand=instructor($select=id,givenName,familyName,category)))')
            .orderBy('course/name')
            .thenByDescending('semester')
            .thenByDescending('gradeYear')
            .take(-1)
            .getItems();
    }

   getDefaultGradeScale():Promise<GradeScale> {
      // get study program
        return this._context.model('students/me/studyProgram')
          .asQueryable().take(-1)
          .getItems()
          .then(studyProgram => {
            // get grade scale
            return this._gradeScaleService.getGradeScale(studyProgram.gradeScale);
        });
    }

  getThesisInfo(): any {
    return this._context.model('students/me/studentthesis')
      .asQueryable()
      .select('thesis,formattedGrade')
      .expand('thesis($expand=instructor($select=familyName))')
      .take(-1)
      .getItems();
  }

  getLastExamPeriod() {
      return this._context.model('students/me/courses')
          .select('gradeYear', 'examPeriod')
          .where('gradeYear').notEqual(null)
          .and('examPeriod').notEqual(null)
          .orderByDescending('gradeYear')
          .thenByDescending('gradePeriod')
          .take(-1)
          .getItem();
  }

  getRecentGrades(): any {
    // get last examination period
    return this.getLastExamPeriod().then((lastExaminationPeriod) => {
      // get last grade year
      let lastGradeYear = lastExaminationPeriod && lastExaminationPeriod.gradeYear && lastExaminationPeriod.gradeYear.id;
      // get last exam period
      let lastExamPeriod = lastExaminationPeriod && lastExaminationPeriod.examPeriod && lastExaminationPeriod.examPeriod.id;
      if (typeof lastGradeYear === 'undefined' || typeof lastExamPeriod === 'undefined') {
        // return empty array
        return Promise.resolve({
          value:[]
        });
      }
      // get courses (expand course attributes, exam instructors)
      return this._context.model('students/me/grades')
        .where('courseExam/year').equal(lastGradeYear)
        .and('courseExam/examPeriod').equal(lastExamPeriod)
        .expand('course', 'courseClass($expand=instructors($expand=instructor($select=id,givenName,familyName,category)))',
          'courseExam($expand=examPeriod,year)')
        .orderBy('course/name')
        .take(-1)
        .getItems();
    });
  }


}
