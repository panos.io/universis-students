import { Component, OnInit } from '@angular/core';
import { GradesService } from '../../services/grades.service';
import {AngularDataContext} from '@themost/angular';
import * as GRADES_CONF from '../../config/grades.config.json';
import {GradeScale} from '../../services/grade-scale.service';
import {LoadingService} from '../../../shared/services/loading.service';

@Component({
  selector: 'app-grades-all',
  templateUrl: './grades-all.component.html',
  styleUrls: ['./grades-all.component.scss']
})
export class GradesAllComponent implements OnInit {

  public semesters: number[] = [];
    /**
     * Gets or sets the grades component configuration
     */
  public configuration: GradesConfiguration = (<GradesConfiguration>GRADES_CONF.default);
    /**
     * Gets or sets the group data based on the selected group configuration
     */
  public groups: any;
    /**
     * Gets or sets a the collection of student course grades
     */
  private data: any;
    /**
     * Gets or sets the selected group configuration
     */
  public selectedGroup = this.configuration.groups[0];

  public defaultGradeScale: GradeScale;

  /* for stats box */
  registeredCourses = 0;
  passedCourses = 0;
  failedCourses = 0;
  passedGradeAverage = '0';

  constructor(private gradesService: GradesService, private loadingService: LoadingService, private contextService: AngularDataContext) {
  }

  ngOnInit() {
    // show loading
    this.loadingService.showLoading();
    // get grades
    this.gradesService.getGradeInfo()
      .then((res) => {
        // get default grade scale
        return this.gradesService.getDefaultGradeScale().then(gradeScale => {
          // set grade scale
          this.defaultGradeScale = gradeScale;
          // set grades
          this.data = res.value;
          // force group change
          this.onChangeGroup(this.selectedGroup.attribute);
          // hide loading
          this.loadingService.hideLoading();
        });
      });
  }

  /**
     *
     * @param {string} groupByAttribute
     */
  getGroupData(groupByAttribute) {
      return this.contextService.model('students/me/courses')
          .select(groupByAttribute, `count(${groupByAttribute}) as count`)
          .groupBy(groupByAttribute)
          .orderBy(groupByAttribute)
          .getItems().then( groups => {
                return groups;
          });
  }

  onSearchKeyDown($event: KeyboardEvent) {
      if ($event.keyCode === 13) {
          $event.preventDefault();
          // get search text
          const searchText = (<HTMLInputElement>$event.target).value;
          if (searchText.length === 0) {
              // apply grouping
              return this.applyGrouping(this.data, this.selectedGroup.attribute);
          }
          // build regular expression
          const searchRegExp = new RegExp(searchText, 'ig');
        const filtered = this.data.slice(0).filter( x => {
                return x.course.name.match(searchRegExp) || x.course.displayCode.match(searchRegExp);
          });
          // apply grouping
          this.applyGrouping(filtered, this.selectedGroup.attribute);
      }
  }

  applyGrouping(data: any, attribute: string) {

      this.getGroupData(attribute).then(result => {
          // get default grade scale
          const gradeScale = this.defaultGradeScale;
          // group grades (by filtering only passed courses)
          result.value.forEach( group => {
              group.grades = data.filter( x => {
                  if (group) {
                      return x[attribute] && (x[attribute].id === group[attribute].id);
                  }
              });
              const passed = group.grades.filter( x => {
                  return x.isPassed;
              });
              const average = passed.length ? passed.map(x => x.grade).reduce((a, b) => a + b) / passed.length : 0;
              group.average = gradeScale.format(average);
          });
          // set groups
          this.groups = result.value;
          // set passed courses
          this.passedCourses = this.data.filter(x => x.isPassed).length;
          // set failed courses
          this.failedCourses = this.data.filter(x => !x.isPassed).length;
          // set sum of courses
          this.registeredCourses = this.passedCourses + this.failedCourses;
          const arr = this.data.filter( x => {
              return x.isPassed;
          });
          // set average of passed courses
          const passedGradeAverage = arr.length ? arr.map(x => x.grade).reduce((a, b) => a + b) / arr.length : 0;
          this.passedGradeAverage = gradeScale.format(passedGradeAverage);

      });

  }


    /**
     * Handles the event of group attribute change
     * @param {*} selected
     */
  onChangeGroup(selected: any) {
        // get courses groups
        const selectedGroup = this.configuration.groups.find(value => {
               return value.attribute === selected;
        });
        // get group attribute
        const attribute = selectedGroup.attribute;
        // ensure grades
        const data = this.data || [];
        // set selected group
        this.selectedGroup = selectedGroup;
        // apply grouping
        this.applyGrouping(data, this.selectedGroup.attribute);
    }

}
