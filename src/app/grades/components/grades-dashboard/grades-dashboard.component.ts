import { Component, OnInit } from '@angular/core';
import { GradesService } from '../../services/grades.service';

@Component({
  selector: 'app-grades-dashboard',
  templateUrl: './grades-dashboard.component.html',
  styleUrls: ['./grades-dashboard.component.scss']
})
export class GradesDashboardComponent implements OnInit {

  courses: any[] = [];

  constructor(private gradesService: GradesService) { }

  ngOnInit() {
      this.gradesService.getAllGrades().then((res) => {
          let latestExamPeriod = res.value[0].courseExam.examPeriod.id;
          let latestExamYear = res.value[0].courseExam.year.id;
          this.courses = res.value.filter((x)=> {
              return (x && x.courseExam && x.courseExam.examPeriod && x.courseExam.examPeriod.id === latestExamPeriod) &&
                  (x && x.courseExam && x.courseExam.year && x.courseExam.year.id === latestExamYear);
          });
      });
  }

}
